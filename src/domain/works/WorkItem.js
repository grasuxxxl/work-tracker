// @flow
import React from "react";
import { CellMeasurer } from "react-virtualized";
import { Link } from "react-router-dom";
import Paper from "material-ui/Paper";

export default function(
  { cache, items },
  { index, isScrolling, isVisible, key, parent, style }
) {
  const item = items[index];
  return (
    <CellMeasurer
      key={key}
      cache={cache}
      columnIndex={0}
      parent={parent}
      rowIndex={index}
    >
      <div className="WorkItemRow" style={style}>
        <div>
          <Link to={`/work-items/${item.id}`}>
            <Paper className="WorkItem">{item.email}</Paper>
          </Link>
        </div>
      </div>
    </CellMeasurer>
  );
}
