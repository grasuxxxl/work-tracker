// @flow
import React, { Component } from "react";
import {
  List,
  WindowScroller,
  InfiniteLoader,
  CellMeasurerCache,
  AutoSizer
} from "react-virtualized";
import { graphql } from "react-apollo";
import R from "ramda";
import WorkItem from "./WorkItem";

class WorkItems extends Component {
  cache: CellMeasurerCache;

  constructor() {
    super();
    this.cache = new CellMeasurerCache({
      defaultHeight: 100,
      fixedWidth: true
    });
  }

  _isRowLoaded(index: number): boolean {
    const { pagedItems } = this.props;
    return !!pagedItems[index];
  }

  render() {
    const { loadNextPage, pagedItems, totalItems } = this.props;
    const cache = this.cache;
    if (!pagedItems) return <div>"Loading"</div>;

    return (
      <InfiniteLoader
        isRowLoaded={this._isRowLoaded.bind(this)}
        loadMoreRows={loadNextPage}
        rowCount={totalItems}
        threshold={40}
      >
        {({ onRowsRendered, registerChild }) => (
          <WindowScroller>
            {({ height, isScrolling, onChildScroll, scrollTop }) => (
              <AutoSizer disableHeight>
                {({ width }) => (
                  <List
                    className="UsersInfiniteWindowList"
                    ref={registerChild}
                    autoHeight
                    height={height}
                    onRowsRendered={onRowsRendered}
                    isScrolling={isScrolling}
                    onScroll={onChildScroll}
                    scrollTop={scrollTop}
                    rowCount={pagedItems.length}
                    // rowHeight={100}
                    rowHeight={cache.rowHeight}
                    deferredMeasurementCache={cache}
                    width={width}
                    rowRenderer={WorkItem.bind(null, {
                      cache,
                      items: pagedItems
                    })}
                  />
                )}
              </AutoSizer>
            )}
          </WindowScroller>
        )}
      </InfiniteLoader>
    );
  }
}

// $FlowFixMe
import PagedUsers from "../../graphql/Query/PagedUsers.gql";
const WorkItemsWithData = graphql(PagedUsers, {
  options: function(props: PagedUsersQueryVariables) {
    return {
      variables: {
        startIndex: 0,
        endIndex: 10
      }
    };
  },
  props: function({
    data: { loading, usersPaged, totalUsers, fetchMore }
  }: {
    data: PagedUsersQuery & { fetchMore: any, loading: boolean }
  }) {
    return {
      loading,
      pagedItems: usersPaged,
      totalUsers,
      loadNextPage({ startIndex, stopIndex }) {
        if (usersPaged[stopIndex - 1] != undefined) return Promise.resolve();

        return fetchMore({
          variables: {
            startIndex,
            endIndex: R.min(totalUsers, stopIndex + 40 + 20)
          },
          updateQuery(previousResult, { fetchMoreResult }) {
            if (!fetchMoreResult) {
              return previousResult;
            }

            const pagedItems = R.unionWith(
              R.eqBy(R.prop("id")),
              previousResult.pagedItems,
              fetchMoreResult.pagedItems
            );
            return Object.assign({}, previousResult, {
              pagedItems: pagedItems
            });
          }
        });
      }
    };
  }
})(WorkItems);

export default WorkItemsWithData;
