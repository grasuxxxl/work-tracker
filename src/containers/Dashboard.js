// @flow
import React from "react";

import WorkItems from "../domain/works/WorkItems.js";

export default function() {
  return <WorkItems />;
}
