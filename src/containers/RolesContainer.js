// @flow
import React from 'react';
import { graphql } from 'react-apollo';
import { Grid } from 'react-virtualized';
import AddRole from '../components/AddRole.js';
// $FlowFixMe
import AllRolesQuery from '../graphql/Query/RoleContainer.gql';

function cellRenderer(roles, { columnIndex, key, rowIndex, style }) {
  const body = columnIndex === 0
        ? roles[rowIndex].name
        : roles[rowIndex].id;

  return (
    <div key={key} style={style}>
      { body }
    </div>
  );
}

function RolesGrid({ roles }) {
  return (
    <div>
      <Grid
        cellRenderer={cellRenderer.bind(null, roles)}
        columnCount={2}
        columnWidth={290}
        height={300}
        rowCount={roles.length}
        rowHeight={30}
        width={600} />
    </div>
  );
}

function RolesContainer({ data: { roles, loading, error } }) {
  var gridOrLoader = loading
        ? "Loading roles"
        : <RolesGrid roles={roles}/>;
  gridOrLoader = error == undefined
    ? gridOrLoader
    : "Error loading roles"


  return (
    <div>
      <h1>Roles</h1>
      <AddRole />
      { gridOrLoader }
    </div>
  );
};

export default graphql(AllRolesQuery)(RolesContainer);
