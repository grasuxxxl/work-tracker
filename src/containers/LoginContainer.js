// @flow
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';

function onLogin(username: string, password: string) {
  return fetch('/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    credentials: 'include',
    body: `username=${username}&password=${password}` }
  )
    .then(response => response.status)
    .then(result => {
      console.log(result)
    });
}

class LoginContainer extends Component {
  state: {
      username: string,
      password: string
  }

  constructor() {
    super();
    this.state = {
      username: '',
      password: ''
    }
  }

  render () {
    const { username, password } = this.state;

    const { history } = this.props;
    const queryStrings = new URLSearchParams(window.location.search);
    const redirectTo = queryStrings.get('redirect') || '/';

    return (
      <div>
        <TextField
          floatingLabelText="Email"
          onChange={e => this.setState({ username: e.target.value })}
          name="username"
          value={username} />
        <br />
        <TextField
          floatingLabelText="Password"
          value={ password }
          onChange={ e => this.setState({ password: e.target.value })}
          name="password"
          type="password" />
        <br />
        <Button raised
          onTouchTap={() => onLogin(username, password).then(() => history.replace(redirectTo))}>Login</Button>
      </div>
    );
  }
};

LoginContainer.contextTypes = {
  router: PropTypes.object
};

export default withRouter(LoginContainer);
