// @flow
import React from 'react';
import R from 'ramda';
import { Grid } from 'react-virtualized';
import { graphql } from 'react-apollo';
import AddCustomer from '../components/AddCustomer.js';
// $FlowFixMe
import AllCustomersQuery from '../graphql/Query/AllCustomers.gql';

const noOfKeys: (Object) => number = R.pipe(R.keys, R.length);

function TextCell({ item, itemKey }) {
  return (
    <div>
      { item[itemKey] }
    </div>
  );
}

function CellWithKey({ Component, itemKey }) {
  return function (props) {
    const { style } = props;
    return (
      <div style={style}>
        <Component itemKey={itemKey} {...props} />
      </div>
    );
  };
}

const cellMap = {
  '0': CellWithKey({ Component: TextCell, itemKey: 'id' }),
  '1': CellWithKey({ Component: TextCell, itemKey: 'name' })
};

function cellRenderer(customers, { columnIndex, key, rowIndex, style}) {
  let Component = cellMap[columnIndex];
  let customer = customers[rowIndex];
  return <Component key={key} style={style} item={customer} />;
}

function CustomersGrid({ customers }) {
  return (
    <div>
      <Grid
        cellRenderer={cellRenderer.bind(null, customers)}
        columnCount={noOfKeys(cellMap)}
        columnWidth={200}
        height={300}
        rowCount={ customers.length }
        rowHeight={30}
        width={600} />
    </div>
  );
}


function CustomersGridContainer({ data: { loading, customers }}) {
  const customersGrid = loading ? '' : <CustomersGrid customers={customers} />;
  return (
    <div>
      <h1>Customers</h1>
      <AddCustomer />
      { customersGrid }
    </div>
  );
}

const CustomersGridContainerWithData = graphql(AllCustomersQuery)(CustomersGridContainer);

export default CustomersGridContainerWithData;
