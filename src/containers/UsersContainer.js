// @flow
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import R from 'ramda';
import {
  Grid, List, Collection, WindowScroller, InfiniteLoader,
  CellMeasurer, CellMeasurerCache, AutoSizer } from 'react-virtualized';
import { Link } from 'react-router-dom';
// $FlowFixMe
import AllUsers from '../graphql/Query/AllUsers.gql';


function cellRenderer(users, { columnIndex, key, rowIndex, style }: { rowIndex: number, columnIndex: number, key: string, style: any }) {
    var body = '';
    switch (columnIndex) {
      case 0:
        body = users[rowIndex].id;
        break;
      case 1:
        body = users[rowIndex].email;
        break;
      case 2:
        body = users[rowIndex].phone;
        break;
      default:
        throw new Error('Column count not supported');
    }

    // const body = columnIndex === 0 ? users[rowIndex].email : users[rowIndex].id;
    return (
      <div style={style} key={key}>
        { body }
        </div>
    );
}

function UsersGrid({ users }) {
  return (
    <div>
      <Link to="/users/add">Add User</Link>
      <Grid
        cellRenderer={cellRenderer.bind(null, users)}
        columnCount={3}
        columnWidth={200}
        height={300}
        rowCount={users.length}
        rowHeight={30}
        width={600} />
    </div>
  );
}

function UsersGridContainer(props: { data: { loading: boolean } & AllUsersQuery }) {
  const { loading = false, users = [] } = props.data || {};

  const usersComponent = loading ? "Loading users" : <UsersGrid users={users} />;
  return (
    <div>
      <h1>Users</h1>
      { usersComponent }
    </div>
  );
}

export default graphql(AllUsers)(UsersGridContainer);
