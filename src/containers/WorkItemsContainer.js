// @flow
import React, { Component } from 'react';
import WorkItems from '../components/WorkItems';
import cache from '../model/cache';

class WorkItemsContainer extends Component {
  state: {
    workItem: any
  }
  
  constructor () {
    super();
    this.state = {
      workItem: undefined
    };
  }

  componentDidMount () {
    cache.get('workItems[0].title')
      .then(function ({ json }) {
        let workItem = json.workItems[0];
        console.log(workItem);
        this.setState({
          workItem: workItem
        });
      }.bind(this));
  }

  isLoading() {
    return !this.state.workItem;
  }
  loader() {
    return (
      <div>Loading...</div>
    );
  }
  workItems () {
    let { title } = this.state.workItem;
    
    return (
      <WorkItems title={title} />
    );
  }
  
  render () {
    return this.isLoading() ? this.loader() : this.workItems();
  }
}

export default WorkItemsContainer;



