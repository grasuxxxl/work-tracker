// @flow

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { ApolloProvider } from 'react-apollo';

import { client } from './graphql/index';
import App from './App';
import WorkItemsContainer from './containers/WorkItemsContainer';
import UsersContainer from './containers/UsersContainer';
import RolesContainer from './containers/RolesContainer';
import AddUser from './components/AddUser.js';
import AddRole from './components/AddRole.js';
import UserToRoles from './components/UserToRoles.js';
import CustomersContainer from './containers/CustomersContainer.js';

import 'react-virtualized/styles.css';
import 'react-select/dist/react-select.css';
import './index.css';


ReactDOM.render((
  <ApolloProvider client={client}>
    <Router>
      <App />
    </Router>
  </ApolloProvider>
), document.getElementById('root'));


// =======
// if ('serviceWorker' in navigator) {
//   window.addEventListener('load', function() {
//     navigator.serviceWorker.register('/sw.js').then(function(registration) {
//       // Registration was successful
//       console.log('ServiceWorker registration successful with scope: ', registration.scope);
//     }, function(err) {
//       // registration failed :(
//       console.log('ServiceWorker registration failed: ', err);
//     });
//   });
// }
