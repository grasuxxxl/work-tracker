// @flow
import React, { Component } from "react";
import { Route, Link } from "react-router-dom";
import { MuiThemeProvider, createMuiTheme } from "material-ui/styles";
import Paper from "material-ui/Paper";
import Menu, { MenuItem } from "material-ui/Menu";
import List, { ListItem, ListItemIcon, ListItemText } from "material-ui/List";
import Drawer from "material-ui/Drawer";
import Button from "material-ui/Button";
import Transition from "react-transition-group/Transition";
import TransitionGroup from "react-transition-group/TransitionGroup";
import Header from "./components/Header.js";
import User from "./components/User.js";

import UsersContainer from "./containers/UsersContainer";
import AddUser from "./components/AddUser.js";
import UserToRoles from "./components/UserToRoles.js";

import RolesContainer from "./containers/RolesContainer";
import AddRole from "./components/AddRole.js";

import CustomersContainer from "./containers/CustomersContainer.js";
import LoginContainer from "./containers/LoginContainer.js";

import UsersInfiniteWindowMasonery from "./components/UsersInfiniteWindowMasonery.js";
import UsersInfiniteWindowList from "./components/UsersInfiniteWindowList.js";

import HeaderAndDrawer from "./components/HeaderAndDrawer.js";

import Dashboard from "./containers/Dashboard";

import "./App.css";

const theme = createMuiTheme({
  palette: {
    // type: 'dark', // Switching the dark mode on is a single property value change.
  }
});

class App extends Component {
  state = {
    menuOpened: Boolean
  };

  constructor() {
    super();
    this.state = {
      menuOpened: false
    };
  }
  // component={UsersInfiniteWindowList}
  closeMenu() {
    this.setState({ menuOpened: false });
  }

  render() {
    const { menuOpened } = this.state;

    return (
      <MuiThemeProvider theme={theme}>
        <div className="App">
          <Route
            exact
            path="/users-exp"
            component={UsersInfiniteWindowMasonery}
          />

          <Route path="/dashboard" component={Dashboard} />

          <Route
            exact
            path="/users"
            render={props => {
              return (
                <HeaderAndDrawer title="Add User">
                  <UsersInfiniteWindowList {...props} key="users" />
                </HeaderAndDrawer>
              );
            }}
          />

          <Route
            path="/users/:userId"
            render={props => {
              const { match } = props;
              return <User id={match.params.userId} />;
            }}
          />

          <Route
            path="/users/add"
            render={props => {
              return (
                <HeaderAndDrawer title="Add User">
                  <AddUser {...props} key="add-user" />
                </HeaderAndDrawer>
              );
            }}
          />

          <Route path="/users/roles" component={UserToRoles} />

          <Route
            exact
            path="/roles"
            render={props => {
              return (
                <HeaderAndDrawer title="Roles">
                  <RolesContainer {...props} key="roles" />
                </HeaderAndDrawer>
              );
            }}
          />
          <Route path="/roles/add" component={AddRole} />

          <Route path="/customers" component={CustomersContainer} />

          <Route path="/login" component={LoginContainer} />
          <Route path="/success" component={() => <div> Success </div>} />
          <Route path="/failure" component={() => <div> Failure </div>} />
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
