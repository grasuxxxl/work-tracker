import falcor from 'falcor';

const cache = new falcor.Model({
  cache: {
    workItems: [
      { id: 1, title: 'Item 1' }
    ]
  }
});


export default cache;
