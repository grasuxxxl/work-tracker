// @flow
import React from 'react';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';
import logo from '../logo.svg';

export default function ({ onShowMenu, title }: { onShowMenu: Function, title: string }) {

  return (
    <AppBar position="static">
      <Toolbar>
        <IconButton aria-label="Menu" onClick={onShowMenu}>
          <MenuIcon />
        </IconButton>
        <Typography type="title">
          { title }
        </Typography>
      </Toolbar>
    </AppBar>
  );
};
