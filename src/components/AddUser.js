// @flow
import type { AddComponentProps } from './AddComponent.js';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AddComponent from './AddComponent';
import { graphql } from 'react-apollo';
import Select from '../Controls/MaterialSelect.js';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import { MenuItem } from 'material-ui/Menu';
import Dialog from 'material-ui/Dialog';
import { mutationErrorHandler } from './error_utils/index.js';
import { validateUserResult } from '../../common/validations/index.js';

// $FlowFixMe
import AllUsers from '../graphql/Query/AllUsers.gql';
// $FlowFixMe
import CreateUser from '../graphql/Mutation/CreateUser.gql';

type AddUserProps = {
  mutate: ({ refetchQueries: [any], variables: CreateUserMutationVariables }) => Promise<any>
} & AddComponentProps;

class AddUser extends Component {
  state: {
    email: string,
    phone: string,
    sex: string,
    password: string,
    notes: string,
    errors: {
      email: Array<string>,
      phone: Array<string>,
      sex: Array<string>,
      password: Array<string>,
      notes: Array<string>
    }
  }
  textInput: HTMLInputElement
  props: AddUserProps

  constructor() {
    super();
    this.state = {
      email: '',
      phone: '',
      sex: '',
      password: '',
      notes: '',
      opened: false,
      errors: {
        email: [],
        phone: [],
        sex: [],
        password: [],
        notes: []
      }
    };
  };

  hasError(errors) {
    return errors.length > 0;
  }

  isValid(): boolean {
    const { validateFor } = this.props;
    return !validateFor.call(this, ['email', 'phone', 'sex', 'password', 'notes']);
  }

  onSave() {
    debugger;
    if (!this.isValid()) return;
    const { mutate } = this.props;
    const router = this.context.router;


    mutate({
      refetchQueries: [{
        query: AllUsers
      }],
      variables: {
        user: {
          phone: this.state.phone,
          email: this.state.email,
          password: this.state.password,
          sex: 'Male',
          notes: this.state.notes
        }
      }
    })
    .then(function ({ data }) {
      router.history.push('/users');
    })
    .catch(mutationErrorHandler.bind(this));
  };

  render() {
    const { onBlur, onChange, onSelectChange } = this.props;
    const { email, phone, sex, password, notes } = this.state;
    const { errors } = this.state;
    const options = [
      { value: 'Male', label: 'Male' },
      { value: 'Female', label: 'Female' }
    ];

    return (
      <div>
        <TextField
          label="Email"
          onChange={onChange.bind(this, 'email')}
          onBlur={onBlur.bind(this, 'email')}
          type="email"
          value={email}
          helperText={errors.email.join(', ')}
          error={this.hasError(errors.email)}
          autoFocus /><br />
        <TextField
          label="Password"
          onChange={onChange.bind(this, 'password')}
          onBlur={onBlur.bind(this, 'password')}
          value={password}
          type="password"
          helperText={errors.password.join(', ')}
          error={this.hasError(errors.password)} /><br />
        <TextField
          label="Phone"
          onChange={onChange.bind(this, 'phone')}
          onBlur={onBlur.bind(this, 'phone')}
          value={phone}
          type="tel"
          helperText={errors.phone.join(', ')}
          error={this.hasError(errors.phone)} /><br />
        <TextField
          label="Notes"
          onChange={onChange.bind(this, 'notes')}
          onBlur={onBlur.bind(this, 'notes')}
          value={notes}
          helperText={errors.notes.join(', ')}
          error={this.hasError(errors.notes)} /><br />
        <Select
          value={sex}
          errorText={errors.sex.join(', ')}
          options={options}
          onChange={onSelectChange.bind(this, 'sex')}
          onBlur={onBlur.bind(this, 'sex')} /><br />

        <Button raised onClick={this.onSave.bind(this)}>Save</Button>
      </div>
    );
  };
};

AddUser.contextTypes = {
  router: PropTypes.object.isRequired
};

export default graphql(CreateUser)(AddComponent(AddUser, validateUserResult));
