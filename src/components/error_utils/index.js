// @flow
import R from 'ramda';

export function mutationErrorHandler(error: { graphQLErrors: Array<any> }) {
  error.graphQLErrors.forEach(function (error) {
    const errors = error.data;
    if (!errors) {
      // this.setState({ opened: true });

      return;
    };
    this.setState({
      errors: Object.assign(this.state.errors, errors)
    })
  }.bind(this));
}

export function validateFor(keys: Array<string>, validationFunc: (any) => any) {
  const validationResult = validationFunc(R.pick(keys, this.state));
  const isValid = !validationResult;

  if (!isValid) {
    this.setState({
      errors: Object.assign(this.state.errors, R.pick(keys, validationResult))
    })
  }
  return validationResult
}
