import React from 'react';
import { MultiGrid, CellMeasurer, CellMeasurerCache, AutoSizer } from 'react-virtualized';
import { graphql } from 'react-apollo';
import UsersInRoles from '../graphql/Query/UsersInRoles.gql';

import RoleCell from './RoleCell';
import './UserToRoles.css';

function UserCell ({ user }) {
  return (
    <div className="UserCell">
      { user.email }
    </div>
  );
}

const cache = new CellMeasurerCache({
  defaultWidth: 140,
  fixedWidth: true
});

function cellRenderer({ users, roles }, { columnIndex, key, rowIndex, style, parent }) {
  const user = users[rowIndex - 1];
  const role = roles[columnIndex - 1];
  var body,
      className = '';
  if (rowIndex === 0) {
    if (columnIndex === 0) {
      body = <div>Users</div>;
    } else {
      body = <div>{role && role.name}</div>;
    }
  } else {
    className = 'UserToRoles-Cell';
    if (columnIndex === 0) {
      body = <UserCell user={user} />;
    } else {
      body = <RoleCell user={user} role={role} />;
    }
  }

  return (
      <div key={key} style={style}>
        <div className={className}>
          { body }
        </div>
      </div>
  );
};

function UserInRole ({ users, roles }) {
  const columnCount = roles.length + 1;
  const rowCount = users.length + 1;
  const STYLE_TOP_LEFT_GRID = {
    borderRight: '2px solid #aaa',
    fontWeight: 'bold'
  }


  return (
    <AutoSizer disableHeight={true}>
      {({ width }) => (
        <MultiGrid
          className="UserToRoles"
          scrollToRow={0}
          scrollToColumn={0}
          cellRenderer={cellRenderer.bind(null, {users, roles })}
          height={500}
          columnWidth={cache.defaultWidth}
          columnCount={columnCount}
          fixedColumnCount={1}
          fixedRowCount={1}
          rowHeight={57}
          rowCount={rowCount}
          width={width}
          styleBottomLeftGrid={ STYLE_TOP_LEFT_GRID } />
      )}
    </AutoSizer>
  );
};



function UserInRoleContainer ({ data: {users, roles, loading } }) {
  const gridOrLoader = loading
        ? 'Loading'
        : <UserInRole roles={roles} users={users} />;

  return (
    <div>
      <h1>User to roles</h1>
      { gridOrLoader }
    </div>
  );
};

export default graphql(UsersInRoles)(UserInRoleContainer);
