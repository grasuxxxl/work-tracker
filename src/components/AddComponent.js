// @flow

import React from 'react';
import { validateFor as baseValidateFor } from './error_utils/index.js';

export type AddComponentProps = {
  onBlur: string => void,
  onChange: (string, SyntheticInputEvent) => void,
  validateFor: Array<string> => any,
  onSelectChange: (string, ?any) => void
};


export default function (ComponentToWrap: any, validationFunction: any) {

  function validateFor(keys: Array<string>) {
    return baseValidateFor.call(this, keys, validationFunction);
  }

  function onBlur(key: string) {
    this.setState({
      errors: Object.assign(this.state.errors, {
        [key]: []
      })
    });
    validateFor.call(this, [key]);
  }

  function onChange(key: string, { target }: SyntheticInputEvent) {
    this.setState({
      [key]: target.value
    });
  }

  function onSelectChange(key: string, selectedItem: ?any) {
    const value = (selectedItem && selectedItem.value) || ''
    this.setState({
      [key]: value,
      errors: Object.assign(this.state.errors, {
        [key]: []
      })
    }, () => validateFor.call(this, [key]));
  }

  return function (props: any) {
    const addProps: AddComponentProps = {
      onBlur,
      onChange,
      onSelectChange,
      validateFor
    };
    var newProps = Object.assign({}, props, addProps);

    return <ComponentToWrap {...newProps} />;
  }
}
