// @flow
import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import { mutationErrorHandler, validateFor as validateForBase } from './error_utils/index.js';
import { validateRoleResult } from '../../common/validations/index.js';

// $FlowFixMe
import AllRolesQuery from '../graphql/Query/RoleContainer.gql';
// $FlowFixMe
import CreateRole from '../graphql/Mutation/CreateRole.gql';

class AddRole extends Component {
  state: {
    name: string,
    errors: {
      name: Array<string>
    }
  }

  props: {
    mutate: ({ refetchQueries: [any], variables: CreateRoleMutationVariables }) => Promise<any>
  }

  constructor() {
    super();
    this.state = {
      name: '',
      errors: {
        name: []
      }
    };
  }

  hasError(errors) {
    return errors.length > 0;
  }

  onChange({ target }: SyntheticInputEvent) {
    this.setState({ name: target.value });
  }

  onFocus() {

  }

  onBlur(key: string) {
    this.setState({
      errors: Object.assign(this.state.errors, {
        [key]: []
      })
    });
    this.validateFor([key]);
  }

  validateFor(keys: Array<string>) {
    return validateForBase.call(this, keys, validateRoleResult);
  }

  isValid(): boolean {
    return !this.validateFor(['name']);
  }

  onSave() {
    if (!this.isValid()) return;
    const { mutate } = this.props;

    mutate({
      refetchQueries: [{
        query: AllRolesQuery
      }],
      variables: {
        role: {
          name: this.state.name
        }
      }
    })
    .then(() => {
      this.setState({name: ''});
      this.nameInput.focus();
    })
    .catch(mutationErrorHandler.bind(this));
  }

  render() {
    const { name } = this.state;
    const { errors } = this.state;

    return (
      <div>
        <TextField
          autoFocus={true}
          onKeyPress={(ev) => {
            if (ev.key === 'Enter') {
              ev.preventDefault();
              this.onSave();
            }
          }}
          inputRef={input => this.nameInput = input }
          label="Name"
          onChange={this.onChange.bind(this)}
          onFocus={this.onFocus.bind(this)}
          onBlur={this.onBlur.bind(this, 'name')}
          error={this.hasError(errors.name)}
          helperText={errors.name.join(', ')}
          value={name} /><br />
        <Button raised onClick={this.onSave.bind(this)}>Save</Button>
      </div>
    );
  }
};

export default graphql(CreateRole)(AddRole);
