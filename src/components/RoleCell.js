// @flow
import React, { Component } from 'react';
import R from 'ramda';
import { graphql } from 'react-apollo';
import Checkbox from 'material-ui/Checkbox';

// $FlowFixMe
import UsersInRoles from '../graphql/Query/UsersInRoles.gql';
// $FlowFixMe
import ToogleUserToRole from '../graphql/Mutation/ToogleUserToRole.gql';

import './RoleCell.css';

function containsRole(role, user): boolean {
  return (R.any(r => r.id === role.id, user.roles));
}

class RoleCell extends Component {
  state: {
    checked: boolean
  };

  constructor(props) {
    super(props);

    const { user, role } = props;
    this.state = {
      checked: containsRole(role, user)
    };
  }

  componentWillReceiveProps(nextProps) {
    const { user, role } = nextProps;
    this.setState({
      checked: containsRole(role, user)
    });
  }

  onRoleChange({user, role, mutate}, event) {
    this.setState({ checked: event.target.checked });
    mutate({
      refetchQueries: [{
        query: UsersInRoles
      }],
      variables: { userId: user.id, roleId: role.id }
    });
  }

  render() {
    const { user, role, mutate } = this.props;
    const { checked } = this.state;

    return (
      <div className="RoleCell">
        <div className="RoleCell-InputContainer">
          <Checkbox checked={checked} onClick={this.onRoleChange.bind(this, {user, role, mutate})} />
        </div>
      </div>
    );
  }
}

export default graphql(ToogleUserToRole)(RoleCell);
