// @flow
import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import R from 'ramda';
import {
  Masonry, WindowScroller, InfiniteLoader,
  CellMeasurer, CellMeasurerCache, AutoSizer,
  createMasonryCellPositioner } from 'react-virtualized';
// $FlowFixMe
import PagedUsers from '../graphql/Query/PagedUsers.gql';

function UserListItem(cache, users, columnWidth, { index, isScrolling, isVisible, key, parent, style }) {
  const user = users[index];

  // style.width = columnWidth;
  style.backgroundColor = 'red';
  return (
    <CellMeasurer
      key={key}
      cache={cache}
      columnIndex={0}
      parent={parent}
      rowIndex={index}
    >
      <div style={style}>
        {user.notes}
        <hr />
      </div>
    </CellMeasurer>

  )
}

class UsersInfiniteWindowMasonery extends Component {
  cache: CellMeasurerCache

  constructor() {
    super();

    this.cache = new CellMeasurerCache({
      defaultHeight: 100,
      defaultWidth: 100,
      fixedWidth: true
    });
  }

  _getCellPositioner() {
    return this.cellPositioner;
  }

  _getCache() {
    return this.cache;
  }

  _createCellPositionerAndReturnColumnWidth(width: number) {
    if (width == 0) return;
    if (this.cellPositioner) return;

    const spacer = 10;
    const columnCount = 3;
    const spacerTotalSpace = spacer * (columnCount - 1);
    const columnWidth = (width - spacerTotalSpace) / columnCount;

    this.cache = new CellMeasurerCache({
      defaultHeight: 100,
      defaultWidth: columnWidth,
      fixedWidth: true
    });
    this.cellPositioner = createMasonryCellPositioner({
      cellMeasurerCache: this.cache,
      columnCount: columnCount,
      columnWidth: columnWidth,
      spacer: spacer
    });



    return columnWidth;
  }

  _isRowLoaded(index: number): boolean {
    const { pagedUsers } = this.props;
    return !!pagedUsers[index];
  }

  render () {
    const { loadNextPage, pagedUsers, totalUsers } = this.props;
    // const cache = this.cache;
    const getCellPositioner = this._getCellPositioner.bind(this);
    const getCache = this._getCache.bind(this);
    const createCellPositionerAndReturnColumnWidth = this._createCellPositionerAndReturnColumnWidth.bind(this);
    if (!pagedUsers) return <div>"Loading"</div>;

    return (
      <InfiniteLoader
        isRowLoaded={this._isRowLoaded.bind(this)}
        loadMoreRows={loadNextPage}
        rowCount={totalUsers}
        threshold={40}>
        {({onRowsRendered, registerChild}) => (
          <WindowScroller>
            {({ height, isScrolling, onChildScroll, scrollTop }) => (
              <AutoSizer disableHeight>
              {({ width }) => {
                const columnWidth = createCellPositionerAndReturnColumnWidth(width);
                const cellPositioner = getCellPositioner();
                const cache = getCache();
                return (
                  <Masonry
                    cellCount={pagedUsers.length}
                    cellMeasurerCache={cache}
                    cellPositioner={cellPositioner}
                    cellRenderer={UserListItem.bind(null, cache, pagedUsers, columnWidth)}
                    height={height}
                    width={width}
                    onCellsRendered={onRowsRendered}
                    onScroll={onChildScroll}
                    ref={registerChild}
                    autoHeight
                    // height={height}
                    // onRowsRendered={onRowsRendered}
                    // isScrolling={isScrolling}
                    // onScroll={onChildScroll}
                    scrollTop={scrollTop}
                    // rowCount={pagedUsers.length}
                    // rowHeight={100}
                    // rowHeight={cache.rowHeight}
                    // deferredMeasurementCache={cache}
                    // width={width}
                    //rowRenderer={UserListItem.bind(null, cache, pagedUsers)}
                    >
                </Masonry>
                )
              }}
              </AutoSizer>
            )}
          </WindowScroller>
        )}
      </InfiniteLoader>
    )
  }
}

const UsersInfiniteWindowMasoneryWithData = graphql(PagedUsers, {
  options: function (props: PagedUsersQueryVariables) {
    return {
      variables: {
        startIndex: 0,
        endIndex: 10
      }
    }
  },
  props: function ({ data: { loading, usersPaged, totalUsers, fetchMore } }: { data: PagedUsersQuery & { fetchMore: any, loading: boolean } }) {
    return {
      loading,
      pagedUsers: usersPaged,
      totalUsers,
      loadNextPage({ startIndex, stopIndex}) {
        if (usersPaged[stopIndex - 1] != undefined) return Promise.resolve();

        return fetchMore({
          variables: {
            startIndex,
            endIndex: R.min(totalUsers, stopIndex + 40 + 20)
          },
          updateQuery (previousResult, data) {
            var fetchMoreResult = data.fetchMoreResult;
            if (!fetchMoreResult) { return previousResult; }

            const usersPaged = R.unionWith(R.eqBy(R.prop('id')), previousResult.usersPaged, fetchMoreResult.usersPaged);
            return Object.assign({}, previousResult, {
              usersPaged
            });
          }
        });
      }
    };
  }
})(UsersInfiniteWindowMasonery);

export default UsersInfiniteWindowMasoneryWithData;
