// @flow
import React, { Component } from 'react';

type Props = {
  title: string
};

class WorkItems extends Component {
  props: Props
  
  render () {
    let { title } = this.props;
    
    return (
      <div>
        <h3>{ title }</h3>
      </div>
    );
  }
}

export default WorkItems;
