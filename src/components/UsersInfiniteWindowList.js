// @flow
import React, { Component } from "react";
import { graphql } from "react-apollo";
import R from "ramda";
import {
  List,
  WindowScroller,
  InfiniteLoader,
  CellMeasurer,
  CellMeasurerCache,
  AutoSizer
} from "react-virtualized";
import Paper from "material-ui/Paper";
import Button from "material-ui/Button";
import { Link } from "react-router-dom";
import "./UsersInfiniteWindowList.css";
// $FlowFixMe
import PagedUsers from "../graphql/Query/PagedUsers.gql";

function UserListItem(
  cache,
  users,
  { index, isScrolling, isVisible, key, parent, style }
) {
  const user = users[index];
  return (
    <CellMeasurer
      key={key}
      cache={cache}
      columnIndex={0}
      parent={parent}
      rowIndex={index}
    >
      <div className="User-container" style={style}>
        <div>
          <Link to={`/users/${user.id}`}>
            <Paper className="User">{user.email}</Paper>
          </Link>
        </div>
      </div>
    </CellMeasurer>
  );
}

class UsersListTest extends Component {
  cache: CellMeasurerCache;

  constructor() {
    super();
    this.cache = new CellMeasurerCache({
      defaultHeight: 100,
      fixedWidth: true
    });
  }

  _isRowLoaded(index: number): boolean {
    const { pagedUsers } = this.props;
    return !!pagedUsers[index];
  }

  render() {
    const { loadNextPage, pagedUsers, totalUsers } = this.props;
    const cache = this.cache;
    if (!pagedUsers) return <div>"Loading"</div>;

    return (
      <InfiniteLoader
        isRowLoaded={this._isRowLoaded.bind(this)}
        loadMoreRows={loadNextPage}
        rowCount={totalUsers}
        threshold={40}
      >
        {({ onRowsRendered, registerChild }) => (
          <WindowScroller>
            {({ height, isScrolling, onChildScroll, scrollTop }) => (
              <AutoSizer disableHeight>
                {({ width }) => (
                  <List
                    className="UsersInfiniteWindowList"
                    ref={registerChild}
                    autoHeight
                    height={height}
                    onRowsRendered={onRowsRendered}
                    isScrolling={isScrolling}
                    onScroll={onChildScroll}
                    scrollTop={scrollTop}
                    rowCount={pagedUsers.length}
                    // rowHeight={100}
                    rowHeight={cache.rowHeight}
                    deferredMeasurementCache={cache}
                    width={width}
                    rowRenderer={UserListItem.bind(null, cache, pagedUsers)}
                  />
                )}
              </AutoSizer>
            )}
          </WindowScroller>
        )}
      </InfiniteLoader>
    );
  }
}

const UserListTestWithData = graphql(PagedUsers, {
  options: function(props: PagedUsersQueryVariables) {
    return {
      variables: {
        startIndex: 0,
        endIndex: 10
      }
    };
  },
  props: function({
    data: { loading, usersPaged, totalUsers, fetchMore }
  }: {
    data: PagedUsersQuery & { fetchMore: any, loading: boolean }
  }) {
    return {
      loading,
      pagedUsers: usersPaged,
      totalUsers,
      loadNextPage({ startIndex, stopIndex }) {
        if (usersPaged[stopIndex - 1] != undefined) return Promise.resolve();

        return fetchMore({
          variables: {
            startIndex,
            endIndex: R.min(totalUsers, stopIndex + 40 + 20)
          },
          updateQuery(previousResult, data) {
            var fetchMoreResult = data.fetchMoreResult;
            if (!fetchMoreResult) {
              return previousResult;
            }

            const usersPaged = R.unionWith(
              R.eqBy(R.prop("id")),
              previousResult.usersPaged,
              fetchMoreResult.usersPaged
            );
            return Object.assign({}, previousResult, {
              usersPaged
            });
          }
        });
      }
    };
  }
})(UsersListTest);

export default UserListTestWithData;
