// @flow
import React, { Component } from "react";
import Header from "./Header.js";
import Drawer from "material-ui/Drawer";
import { Link } from "react-router-dom";
import List, { ListItem, ListItemIcon, ListItemText } from "material-ui/List";

export default class HeaderAndDrawer extends Component {
  state = {
    menuOpened: Boolean
  };

  constructor() {
    super();
    this.state = {
      menuOpened: false
    };
  }
  // component={UsersInfiniteWindowList}
  closeMenu() {
    this.setState({ menuOpened: false });
  }

  render() {
    const { title } = this.props;
    const { menuOpened } = this.state;

    return [
      <Header
        key="header"
        onShowMenu={() => this.setState({ menuOpened: true })}
        title="Users"
      />,
      <Drawer
        key="drawer"
        open={menuOpened}
        onRequestClose={this.closeMenu.bind(this)}
      >
        <List>
          <Link to="/" onClick={this.closeMenu.bind(this)}>
            <ListItem button>
              <ListItemText primary="Dashboard" />
            </ListItem>
          </Link>
          <Link to="/users" onClick={this.closeMenu.bind(this)}>
            <ListItem button>
              <ListItemText primary="Users" />
            </ListItem>
          </Link>
          <Link to="/roles" onClick={this.closeMenu.bind(this)}>
            <ListItem button>
              <ListItemText primary="Roles" />
            </ListItem>
          </Link>
          <Link to="/dashboard" onClick={this.closeMenu.bind(this)}>
            <ListItem button>
              <ListItemText primary="Dashboard" />
            </ListItem>
          </Link>
        </List>
      </Drawer>,
      this.props.children
    ];
  }
}
