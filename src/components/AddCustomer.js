// @flow
import type { AddComponentProps } from './AddComponent.js';

import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Select from '../Controls/MaterialSelect.js';
import { mutationErrorHandler } from './error_utils/index.js';
import AddComponent from './AddComponent';
import { validateCustomerResult } from '../../common/validations/index.js';

import { client } from '../graphql/index'

// $FlowFixMe
import AllCustomersQuery from '../graphql/Query/AllCustomers.gql';
// $FlowFixMe
import CreateCustomer from '../graphql/Mutation/CreateCustomer.gql';

type AddCustomerProps = {
  mutate: ({ refetchQueries: [any], variables: CreateCustomerMutationVariables }) => Promise<any>
} & AddComponentProps;

type AddCustomerState = { ...{|
  name: string,
  errors: {
    name: Array<string>
  }
|},  ...UsersForCustomerQuery};

class AddCustomer extends Component {
  state: AddCustomerState

  props: AddCustomerProps

  constructor() {
    super();
    this.state = {
      name: '',
      users: [],
      errors: {
        name: []
      }
    };
  }

  isValid(): boolean {
    const { validateFor } = this.props;
    return !validateFor.call(this, ['name']);
  }

  onSave() {
    if (!this.isValid()) return;
    const { mutate } = this.props;

    mutate({
      refetchQueries: [{
        query: AllCustomersQuery
      }],
      variables: {
        customer: {
          name: this.state.name
        }
      }
    })
    .then(() => this.setState({name: ''}))
    .catch(mutationErrorHandler.bind(this));
  }

  render() {
    const { onBlur, onChange, onSelectChange } = this.props;
    const { name } = this.state;
    const { errors } = this.state;

    return (
      <div>
        <TextField
          floatingLabelText="Name"
          onChange={onChange.bind(this, 'name')}
          onBlur={onBlur.bind(this, 'name')}
          value={name}
          errorText={errors.name.join(', ')} /><br />


        <Button raised onTouchTap={this.onSave.bind(this)} label="Save" />
      </div>
    );
  }
};

export default graphql(CreateCustomer)(AddComponent(AddCustomer, validateCustomerResult));
