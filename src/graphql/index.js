// @flow
import ApolloClient, { createNetworkInterface } from 'apollo-client';

export const client = new ApolloClient({
  networkInterface: createNetworkInterface({ uri: '/api'}),
  dataIdFromObject: o => o.id
});
