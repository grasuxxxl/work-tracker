import React, { Component } from 'react';
import R from 'ramda';
import Select from 'react-select';

export default class extends Component {
  constructor() {
    super();

    this.state = {
      containerClassName: ''
    };
    // this.errorContainer = document.createElement('div');
    // this.horizontalLineFocus = document.createElement('hr');
  }

  onFocus() {
    this.setState({
      containerClassName: 'is-focused'
    })
  }

  onBlur() {
    const { onBlur } = this.props;
    onBlur();

    this.setState({
      containerClassName: ''
    });
  }

  render() {
    const { containerClassName } = this.state;
    const componentProps = R.pick(['name', 'value', 'options', 'onChange'], this.props);
    const { errorText } = this.props;
    const hasErrorClass = errorText.length === 0 ? '' : 'has-error';

    return (
      <div className={`MaterialSelect ${containerClassName} `}>
        <Select
          className={'MaterialSelect-ReactSelect'}
          onFocus={this.onFocus.bind(this)}

          onBlur={this.onBlur.bind(this)}
          {...componentProps} />
        <div className="line-container">
          <hr className="select-line normal-line" />
          <hr className={`select-line normal-line-focus ${hasErrorClass}`} />
        </div>
        <div className="select-error-container">
          { errorText }
        </div>
      </div>
    )
  }
}
