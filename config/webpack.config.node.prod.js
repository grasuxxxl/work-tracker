var path = require('path');
var paths = require('./paths');
var nodeExternals = require('webpack-node-externals');
var webpack = require('webpack');

module.exports = {
  devtool: 'sourcemap',
  target: 'node',
  node: {
    __dirname: false
  },
  entry: [
    path.resolve(__dirname, '../', 'backend', 'index.js')
  ],
  output: {
    path: path.resolve(__dirname, '../', 'backend-build'),
    filename: '[name].js'
  },
  externals: [nodeExternals()],
  module: {
    loaders: [
      {
        test: /\.js$/,
        // include: [paths.common],
        loader: 'babel'
      }
    ]
  },
  plugins: [
    new webpack.BannerPlugin('require("source-map-support").install();', { raw: true, entryOnly: false })
  ]
};
