import validate from 'validate.js';
import { ValidationError } from '../errors/index.js';

// ============================================================
export function validateCustomerResult(customer) {
  const contraints = {
    name: {
      presence: true
    }
  };

  return validate(customer, contraints);
}

export function validateCustomer(customer) {
  const validationResult = validateCustomerResult(customer);
  if (!validationResult) return;

  throw new ValidationError({
    data: {
      customer: validationResult
    }
  })
}

// ============================================================
export function validateRoleResult(role) {
  const contraints = {
    name: {
      presence: true
    }
  };

  return validate(role, contraints);
}

export function validateRole(role) {
  const validationResult = validateRoleResult(role);
  if (!validationResult) return;

  throw new ValidationError({
    data: {
      role: validationResult
    }
  })
}

// ============================================================
export function validateUserResult(user) {
  const contraints = {
    email: {
      presence: true,
      email: true
    },
    phone: {
      presence: true,
      numericality: {
        onlyInteger: true
      }
    },
    password: {
      presence: true,
    },
    sex: {
      presence: true
    },
    notes: {
      presence: true
    }
  };

  return validate(user, contraints);
}
export function validateUser(user) {

  const validationResult = validateUserResult(user);
  if (!validationResult) return;

  throw new ValidationError({
    data: {
      user: validationResult
    }
  })
}
