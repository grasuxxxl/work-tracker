import { createError } from 'apollo-errors';

export const ValidationError = createError('ValidationError', {
  message: 'Invalid query'
});
