'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('memberships', {
    id: { type: 'bigint', primaryKey: true, autoIncrement: true },
    phone: { type: 'string' },
    user_id: { type: 'bigint' }
  })
  .then(function () {
    return db.addForeignKey('memberships', 'user', 'membership_to_user', {
      'user_id': 'id'
    }, {
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE'
    });
  });
};

exports.down = function(db) {
  return db.removeForeignKey('memberships', 'membership_to_user')
  .then(function () {
    return db.dropTable('memberships');
  });
};

exports._meta = {
  "version": 1
};
