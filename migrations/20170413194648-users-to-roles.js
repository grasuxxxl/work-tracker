'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db, callback) {
  db.createTable('user_in_role', {
    id: { type: 'bigint', primaryKey: true, autoIncrement: true },
    'user_id': { type: 'bigint' },
    'role_id': { type: 'bigint' }
  }, function () {
    let sql = `ALTER TABLE public.user_in_role
                ADD CONSTRAINT unique_user_role UNIQUE (user_id, role_id)`;
    db.runSql(sql, function () {
      db.addForeignKey('user_in_role', 'user', 'user_user_in_role', {
        'user_id': 'id'
      }, {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      }, function () {
        db.addForeignKey('user_in_role', 'role', 'role_user_in_role', {
          'role_id': 'id'
        }, {
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE'
        }, callback);
      });
    });
  });
};

exports.down = function(db, callback) {
  db.removeForeignKey('user_in_role', 'user_user_in_role', function () {
    db.removeForeignKey('user_in_role', 'role_user_in_role', function () {
      db.dropTable('user_in_role', callback);
    });
  });
};

exports._meta = {
  "version": 1
};
