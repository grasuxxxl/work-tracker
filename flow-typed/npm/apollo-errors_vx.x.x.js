// flow-typed signature: 203b19d615e2ea6c2677ecb4e5bbde5a
// flow-typed version: <<STUB>>/apollo-errors_vx.x.x/flow_v0.37.4

/**
 * This is an autogenerated libdef stub for:
 *
 *   'apollo-errors'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'apollo-errors' {
  declare module.exports: {
    formatError: () => void
  };
}
