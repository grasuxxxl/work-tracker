/* @flow */

declare type GraphQLResponseRoot = {
  data?: Schema$Query | Schema$Mutation;
  errors?: Array<GraphQLResponseError>;
}

declare type GraphQLResponseError = {
  message: string;            // Required for all errors
  locations?: Array<GraphQLResponseErrorLocation>;
  [propName: string]: any;    // 7.2.2 says 'GraphQL servers may provide additional entries to error'
}

declare type GraphQLResponseErrorLocation = {
  line: number;
  column: number;
}

declare type Schema$Query = {
  users: Array<Schema$User>;
  customers: ?Array<Schema$Customer>;
}

declare type Schema$User = {
  id: number;
  email: string;
  phone: string;
}

declare type Schema$Role = {
  id: number;
  name: string;
}

declare type Schema$Customer = {
  id: ?number;
  name: ?string;
  contacts: ?Array<Schema$Contact>;
}

declare type Schema$Contact = {
  id: ?number;
  user: ?Schema$User;
}

declare type Schema$Mutation = {
  createUser: Schema$User;
  createCustomer: Schema$Customer;
  toogleUserToRole: ?Schema$MutationResult;
}

declare type Schema$CreateUser = {
  email: string;
  phone: string;
  sex: Sex;
}

declare type Sex = "Male" | "Female";

declare type Schema$CreateRole = {
  name: string;
}

declare type Schema$CreateCustomer = {
  name: string;
}

declare type Schema$MutationResult = {
  success: ?boolean;
}
