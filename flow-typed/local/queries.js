/* @flow */
//  This file was automatically generated and should not be edited.

export type CreateCustomer = {|
  name: string,
|};

export type CreateRole = {|
  name: string,
|};

export type CreateUser = {|
  email: string,
  password: string,
  phone: string,
  notes: string,
  sex: Sex,
|};

export type Sex =
  "Male" |
  "Female";


export type CreateCustomerMutationVariables = {|
  customer: ?CreateCustomer,
|};

export type CreateCustomerMutation = {|
  createCustomer: {|
    id: ?number,
    name: ?string,
  |},
|};

export type CreateRoleMutationVariables = {|
  role: ?CreateRole,
|};

export type CreateRoleMutation = {|
  createRole: {|
    id: number,
    name: string,
  |},
|};

export type CreateUserMutationVariables = {|
  user: ?CreateUser,
|};

export type CreateUserMutation = {|
  createUser: {|
    id: number,
    email: string,
  |},
|};

export type ToogleUserToRoleMutationVariables = {|
  userId: ?number,
  roleId: ?number,
|};

export type ToogleUserToRoleMutation = {|
  toogleUserToRole: ? {|
    success: ?boolean,
  |},
|};

export type CustomersQuery = {|
  customers: ?Array< {|
    id: ?number,
    name: ?string,
  |} >,
|};

export type AllUsersQuery = {|
  users: Array< {|
    id: number,
    email: string,
    phone: string,
  |} >,
|};

export type PagedUsersQueryVariables = {|
  startIndex: number,
  endIndex: number,
|};

export type PagedUsersQuery = {|
  usersPaged: Array< {|
    id: number,
    email: string,
    phone: string,
    notes: string,
  |} >,
  totalUsers: number,
|};

export type AllRolesQuery = {|
  roles: Array< {|
    id: number,
    name: string,
  |} >,
|};

export type UsersForCustomerQuery = {|
  users: Array< {|
    id: number,
    email: string,
  |} >,
|};

export type UsersInRolesQuery = {|
  users: Array< {|
    id: number,
    email: string,
    roles: Array< {|
      id: number,
      name: string,
    |} >,
  |} >,
  roles: Array< {|
    id: number,
    name: string,
  |} >,
|};