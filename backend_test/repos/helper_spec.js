import db from '../../backend/services/db.js';
import test from 'tape';

test.onFinish(function () {
  db.destroy();
});
