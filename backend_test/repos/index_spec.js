import type { User, CreateUser, CreateRole, CreateUserInRole } from '../../backend/models/index.js';

var test = require('tape');
import repo from '../../backend/repos/index.js';
import R from 'ramda';

test('repo', function (t) {

  var user: CreateUser = { email: "ionel" };
  var role: CreateRole = { name: 'god' };

  repo.users.removeAll()
  .then(function () {
    return repo.users.add(user);
  })
  .then(function (userId) {
    let user = repo.users.getById(userId);
    return user;
  })
  .then(function (user) {
    t.deepEqual(Object.keys(user), ['id', 'email'], "returns the proper keys");
    t.equal(user.email, 'ionel', 'sets the proper value to a field');
  })
  .then(function () {
    return repo.users.getAll();
  })
  .then(function (users) {
    t.equal(users.length, 1, 'return only one record after deleting all and inserting one');
  })
  .then(function () {
    return repo.roles.removeAll();
  })
  .then(function () {
    return repo.roles.add(role);
  })
  .then(function (roleId) {
    return repo.roles.getById(roleId);
  })
  .then(function (role) {
    t.deepEqual(Object.keys(role), ['id', 'name'], 'returns the proper keys for role');
    t.equal(role.name, 'god', 'retrieved role name should be equal to the inserted one');
    
    t.end();
  });
});

test('repo users in role', function (t) {
  const users: [CreateUser] = [
    { email: 'ion@bla.com' },
    { email: 'vasile@bla.com' }
  ];

  const roles: [CreateRole] = [
    { name: 'Zeus' },
    { name: 'Apollo' }
  ];

  var firstUser, firstRole, firstUserInFirstRole;
  
  Promise.all([
    repo.users.removeAll(),
    repo.roles.removeAll()
  ]).then(() => Promise.all(
    R.concat(
      R.map(user => repo.users.add(user), users),
      R.map(role => repo.roles.add(role), roles)
    )
  )).then(() => Promise.all([
    repo.users.getAll(),
    repo.roles.getAll()
  ])).then(function ([users, roles]) {
    firstUser = R.head(users);
    firstRole = R.head(roles);
    firstUserInFirstRole = {
      user_id: firstUser.id,
      role_id: firstRole.id
    };
    
    return repo.userInRole.add(firstUserInFirstRole);
  })
  .then(repo.userInRole.getById)
  .then(function (userInRole) { 
    t.equal(R.isEmpty(userInRole),
            false,
            'should create the user-role relationship');

    return repo.userInRole.remove(userInRole);
  })
  .then(function (userInRoleId) {
    t.notEqual(userInRoleId, 0, 'should return the id of the deleted entity');

    return repo.userInRole.toogle(firstUserInFirstRole);
  })
  .then(function (userInRoleId) {
    t.notEqual(userInRoleId, 0, 'should toogle on the role of the user');
    console.log(firstUserInFirstRole);
    return repo.userInRole.toogle(firstUserInFirstRole);
  })
  .then(function (userInRoleId) {
    t.notEqual(userInRoleId, 0, 'should toggle off the role of the user');
    t.end();
  });
});

