// @flow

const port = process.env.PORT || 4000;
var express = require('express');
var passport = require('passport');
var routes = require('./routes');
var LocalStrategy = require('passport-local').Strategy;
var BasicStrategy = require('passport-http').BasicStrategy;

// ==========
import { repoWithTransaction } from './repos/index';
import { perConnectionDb } from './services/db';
import { isValid } from './services/authentication';
// ==========

// ====================================================

passport.use('local', new LocalStrategy(
  function (username, password, done) {
    console.log("Trying to authenticate: ", username);
    const db = perConnectionDb();
    db.transaction(function (transaction) {
      const repo = repoWithTransaction(transaction);
      isValid(repo, { email: username, password })
        .then(valid => {
          console.log(valid)
          valid
            ? done(null, { username: username })
            : done(null, false)
        })
        .catch(() => 1)
        .then(() => transaction.rollback());
    });
  }
));
passport.serializeUser(function (user, done) {
  user ? done(null, user.username) : done(null, null);
});
passport.deserializeUser(function (id, done) {
  done(null, { _id: 1 });
});


var app: express$Application = express();

// Use application-level middleware for common functionality, including
// logging, parsing, and session handling.
app.use(require('morgan')('combined'));
app.use(require('cookie-parser')());
app.use(require('body-parser').urlencoded({ extended: true }));
app.use(require('express-session')({ secret: 'keyboard cat', resave: false, saveUninitialized: false }));

app.use(passport.initialize());
app.use(passport.session());

app.get(
  '/success',
  function (req: any, res: express$Response, next) {
    console.log(req.isAuthenticated());
    if (!req.isAuthenticated()) {
       return res.redirect('/login');
    }
    next();
  }
);

app.post('/login',
         passport.authenticate('local'),
         function (req: express$Request, res: express$Response) {
           res.redirect('/');
         });

routes.initWith(app);

app.listen(port, function () {
  console.log('Listening on port ', port);
});
