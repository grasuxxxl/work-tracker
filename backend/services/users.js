// @flow
import type { RepoColl } from '../repos/index';
import type { User } from '../models/index';

import R from 'ramda';

import { hashPassword } from './authentication';

export function getAllUsers(repo: RepoColl): Promise<Array<Schema$User>> {
  return Promise.all([repo.users.getAll(), repo.memberships.getAll()])
    .then(function ([users, memberships]) {
      return R.map(function (user) {
        const membership = R.find(m => m.user_id == user.id, memberships);
        if (!membership) {
            throw new Error(`Unable to find membership for user with id: ${user.id}`)
        }

        return R.merge(membership, user);
      }, users);

    });
}

export function getPagedUsers(repo: RepoColl, startIndex: number, endIndex: number): Promise<Array<Schema$User>> {
  return Promise.all([repo.users.getPaged(startIndex, endIndex), repo.memberships.getPaged(startIndex, endIndex)])
    .then(function ([users, memberships]) {
      return R.map(function (user) {
        const membership = R.find(m => m.user_id == user.id, memberships);
        if (!membership) {
            throw new Error(`Unable to find membership for user with id: ${user.id}`)
        }

        return R.merge(membership, user);
      }, users);

    });
}

export function addUser(repo: RepoColl, user: { email: string, password: string, password: string, phone: string, sex: 'Male' | 'Female' }): Promise<User> {
  const { password, email, phone, notes } = user;


  return hashPassword(password)
    .then(hashedPassword => {
      return repo.users.add({ email, password: hashedPassword });
    })
    .then(userId => {
      return repo.memberships.add({ phone, user_id: userId, notes: notes })
        .then(() => {
          return repo.users.getById(userId);
        })
    })
}
