// @flow
import type { RepoColl } from '../repos/index';

import { hash, genSalt, compare } from 'bcrypt';

export type Authentication = {
  email: string,
  password: string
};

export function isValid(repo: RepoColl, auth: Authentication): Promise<boolean> {
  const { email, password } = auth;

  return Promise.all([
    repo.users.getWhere({ email })
  ])
  .then(([user]) => {
    console.log(user);
    if (!user) return false;
    return compare(password, user.password);
  })
}

export function hashPassword(password: string): Promise<string> {
  const saltRounds = 10;
  return genSalt(saltRounds)
    .then(hash.bind(null, password));
}
