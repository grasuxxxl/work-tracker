// @flow
import knex from 'knex';

const db = knex({
  client: 'pg',
  connection: {
    host: 'localhost',
    user: 'work-tracker',
    password: 'work-tracker',
    database: 'work-tracker'
  }
});

export default db;
export function perConnectionDb() {
  return db;
}
