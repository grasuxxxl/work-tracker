// @flow

function inc(x: number): number {
  return x + 1;
}

console.log(inc(1));

export default inc;
