// @flow
import type { Repo } from './core.js';
import type {
  Role, CreateRole,
  User, CreateUser
} from '../models/index.js';

export type UsersRepo = {
  getRoles: { id: number } => Promise<Array<Role>>
} & Repo<User, CreateUser>

import R from 'ramda';
import core from './core.js';
import { fields } from '../models/index.js';


function getRoles(transaction: Knex$Transaction, { id }: { id: number }): Promise<Array<Role>> {
    return new Promise(function (resolve, reject) {
      transaction.select.apply(transaction, fields.role)
      .from('role')
      .innerJoin('user_in_role', 'role.id', 'user_in_role.role_id')
      .where('user_in_role.user_id', id)
      .then(function (result) {
        resolve(result);
      });
    });
}

export function users(transaction: Knex$Transaction): UsersRepo {
  var result = R.merge({
    getRoles: getRoles.bind(null, transaction),
  }, core('user', transaction));

  return result
}
