// @flow

import R from 'ramda';

export function hasResults (results: Array<any> | Object): boolean {
  return R.isEmpty(results);
}
