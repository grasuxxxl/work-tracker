// @flow

import type {
  User, CreateUser,
  Role, CreateRole,
  UserInRole, CreateUserInRole,
  Customer, CreateCustomer,
  Membership, CreateMembership } from '../models/index.js';
import type { Repo } from './core.js';
import type { UsersRepo } from './users';

type UserInRoleRepo = {
  toogle: CreateUserInRole => Promise<number>
} & Repo<UserInRole, CreateUserInRole>

export type RepoColl = {
  users: UsersRepo,
  roles: Repo<Role, CreateRole>,
  userInRole: UserInRoleRepo,
  customers: Repo<Customer, CreateCustomer>,
  memberships: Repo<Membership, CreateMembership>
};

import R from 'ramda';
import core from './core.js';
import { hasResults } from './utils.js';

import { users } from './users';
import { userInRole } from './user_in_role';
import db from  '../services/db.js';
import { fields } from '../models/index.js';

export function repoWithTransaction (transaction: Knex$Transaction): RepoColl {
  return {
    users: users(transaction), // core('user', transaction),
    userInRole: userInRole(transaction),
    memberships: core('memberships', transaction),
    roles: core('role', transaction),
    customers: core('customers', transaction),
    memberships: core('memberships', transaction)
  }
};
