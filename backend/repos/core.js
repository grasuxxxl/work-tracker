// @flow

export type Repo<T, C> = {
  getById: number => Promise<T>,
  add: C => Promise<number>,
  getPaged: (startIndex: number, endIndex: number) => Promise<Array<T>>,
  getAll: () => Promise<Array<T>>,
  getAllCount: () => Promise<number>,
  removeAll: () => Promise<any>,
  getAllWhere: any => Promise<Array<T>>,
  getWhere: any => Promise<?T>,
  remove: any => Promise<number>
};

import db from  '../services/db.js';
import { fields } from '../models/index.js';

export default function core<T, C>(table: string, transaction: any): Repo<T, C> {
  return {
    getById: function (id: number): Promise<T> {
      return new Promise(function (resolve, reject) {
        let query = transaction.from(table).where('id', id);
        query.first.apply(query, fields[table])
        .then(function (result: any) {
          resolve(result);
        });
      });
    },
    add: function (entity: C): Promise<number> {
      return new Promise(function (resolve, reject) {
          console.log(`${JSON.stringify(entity)} - ${table}`);
          transaction.into(table)
          .insert(entity, ['id'])
          .then(function (result) {
            let first = result[0];
            resolve(first.id);
          })
          .catch(reject);
      });
    },
    getPaged: function(startIndex, endIndex): Promise<Array<T>> {
      return new Promise(function (resolve, reject) {
          const limit = endIndex - startIndex;
          const query = transaction.from(table)
            .orderBy('id', 'desc')
            .offset(startIndex)
            .limit(limit);

          return query.select.apply(query, fields[table])
            .then(function (result: any) {
              resolve(result);
            })
            .catch(reject);
      });
    },
    getAllCount: function (): Promise<number> {
      return new Promise(function (resolve, reject) {
          const query = transaction.from(table)

          return query.count('id')
            .then(function (result: any) {
              resolve(parseInt(result[0].count));
            })
            .catch(reject);
      });
    },
    getAll: function (): Promise<Array<T>> {
      return new Promise(function (resolve, reject) {
          const query = transaction.from(table)
            .orderBy('id', 'desc');

          return query.select.apply(query, fields[table])
            .then(function (result: any) {
              resolve(result);
            })
            .catch(reject);
      });
    },
    removeAll: function (): Promise<any> {
      return new Promise(function (resolve, reject) {
        db(table)
        .del()
        .then(function (result) {
          resolve(result);
        });
      });
    },
    getAllWhere: function (params: any) {
      return new Promise(function (resolve, reject) {
        const query = db(table).orderBy('id', 'desc')
        query.select.apply(query, fields[table])
        .where(params)
        .then(result => resolve(result))
      });
    },
    getWhere: function (params: any) {
      return new Promise(function (resolve, reject) {
        let query = db(table).where(params);
        query.first.apply(query, fields[table])
        .then(function (result: any) {
          resolve(result);
        })
        .catch(error => reject(error));
      });
    },
    remove: function (entity: any) {
      return new Promise(function (resolve, reject) {
        db(table)
         .where('id', entity.id)
          .del()
          .then(function (result) {
            resolve(entity.id);
          })
        .catch(reject);
      });
    }
  };
};
