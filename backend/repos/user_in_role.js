// @flow
import type { Repo } from './core.js';
import type {
  UserInRole, CreateUserInRole
} from '../models/index.js';

type UserInRoleRepo = {
  toogle: CreateUserInRole => Promise<number>
} & Repo<UserInRole, CreateUserInRole>

import R from 'ramda';
import core from './core.js';
import { fields } from '../models/index.js';
import { hasResults } from './utils.js';



function add (originalAdd: (CreateUserInRole) => Promise<number> ,userInRole: CreateUserInRole): Promise<number> {
  return originalAdd(userInRole)
     .catch(function (error) {
       if (error.constraint === 'unique_user_role') {
         // TODO: fetch the real object and return its id
         return 0;
       }
       throw error;
     })
};

const toogle = function (userInRoleCoreRepo: Repo<UserInRole, CreateUserInRole>, userInRole: CreateUserInRole): Promise<number> {
  return userInRoleCoreRepo.getAllWhere(userInRole)
    .then((results: Array<UserInRole>) => hasResults(results)
          ? userInRoleCoreRepo.add(userInRole)
          : userInRoleCoreRepo.getWhere(userInRole).then(userInRoleCoreRepo.remove));
};

export function userInRole(transaction: Knex$Transaction): UserInRoleRepo {
  var userInRoleCoreRepo: Repo<UserInRole, CreateUserInRole> = core('user_in_role', transaction);
  var originalAdd = userInRoleCoreRepo.add;

  return R.merge(userInRoleCoreRepo, {
    toogle: toogle.bind(null, userInRoleCoreRepo),
    add: add.bind(null, originalAdd)
  });
};
