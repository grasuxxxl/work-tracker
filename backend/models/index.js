// @flow

export type CreateUser = {
  email: string,
  password: string
};

export type User = {
  id: number
} & CreateUser;

export type CreateRole = {
  name: string
};

export type Role = {
    id: number
} & CreateRole;

export type CreateUserInRole = {
  user_id: number,
  role_id: number
};

export type UserInRole = {
  id: number
} & CreateUserInRole;

export type CreateCustomer = {
  name: string
};

export type Customer = {
  id: number
} & CreateCustomer;

export type CreateMembership = {
  phone: string,
  notes: string,
  user_id: number
};
export type Membership = {
  id: number
} & CreateMembership;

export const fields = {
  user: ['id', 'email', 'password'],
  role: ['role.id', 'role.name'],
  userInRole: ['id', 'user_id', 'role_id'],
  customer: ['id', 'name'],
  memberships: ['id', 'phone', 'user_id', 'notes']
};
