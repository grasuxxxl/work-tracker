// @flow
import bodyParser from 'body-parser';
import express from 'express';
import path from 'path';
import { graphqlExpress, graphiqlExpress } from 'graphql-server-express';


import { makeGraphqlExpressOptions } from './graphql_express_knex';

function checkAuthAndRedirect(req: any, res: express$Response, next) {
  console.log('>>>>>>');
  console.log(req.isAuthenticated());
  if (req.isAuthenticated()) return next();
  res.redirect(302, `/login?redirect=${req.path}`);
}

function checkAuth(req: any, res: express$Response, next) {
  if (req.isAuthenticated()) return next();
  res.sendStatus(401);
}

module.exports = {
  initWith: function initWith(app: express$Application) {

    app.use('/api',
    // checkAuth,
    bodyParser.json(), graphqlExpress(makeGraphqlExpressOptions));

    app.use('/api-admin', graphiqlExpress({
      endpointURL: '/api'
    }));

    app.use(express.static(path.join(__dirname, '../build'), { index: false }));
    app.get('/login',
      function (req: express$Request, res: express$Response) {
        res.sendFile(path.join(__dirname, '../build', 'index.html'));
      });

    app.get('/*',
      //checkAuthAndRedirect,
      function (req: express$Request, res: express$Response) {
        res.sendFile(path.join(__dirname, '../build', 'index.html'));
      });
  }
};
