// @flow
import { formatError } from 'apollo-errors';
import schema from '../data/schema.js';

import { perConnectionDb } from '../services/db';

function formatResponse(errorContainer: { imaximix$$hasError: boolean }, gqlResponse: { errors: ?Array<any>}) {
  if (gqlResponse.errors && gqlResponse.errors.length > 0) {
    errorContainer.imaximix$$hasError = true;
  }

  return gqlResponse;
}

function responseFinishHandler(response: express$Response, errorContainer: { imaximix$$hasError: boolean }, trx: Knex$Transaction) {
  if (response.statusCode >= 500 || response.statusCode >= 400) {
    return trx.rollback();
  }

  if (errorContainer.imaximix$$hasError === true) {
    return trx.rollback();
  }

  trx.commit();
}

export function makeGraphqlExpressOptions (request: express$Request, response: express$Response) {
  const db = perConnectionDb();
  var errorContainer = {
    imaximix$$hasError: false
  };

  return new Promise(function (resolve, reject) {
      db.transaction(function (trx) {
        response.on('finish', responseFinishHandler.bind(null, response, errorContainer, trx));
        response.on('close', () => trx.rollback());

        resolve({
          schema,
          formatError,
          formatResponse: formatResponse.bind(null, errorContainer),
          context: {
            transaction: trx
          }
        })
      });
  })
}
