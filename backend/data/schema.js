// @flow

import { makeExecutableSchema } from 'graphql-tools';
import resolvers from './resolvers.js';

export const schema = `
enum Sex {
  Male
  Female
}

type User {
  id: Int!,
  email: String!,
  phone: String!,
  roles: [Role]!,
  notes: String!
}

input CreateUser {
  email: String!,
  password: String!,
  phone: String!,
  notes: String!,
  sex: Sex!
}

input CreateRole {
  name: String!
}

input CreateCustomer {
  name: String!
}

type Role {
  id: Int!,
  name: String!
}

type Customer {
  id: Int,
  name: String,
  contacts: [Contact]
}

type Contact {
  id: Int,
  user: User
}

type Query {
  totalUsers: Int!
  usersPaged(startIndex: Int!, endIndex: Int!): [User]!
  users: [User]!,
  roles: [Role]!,
  customers: [Customer]
}

type MutationResult {
  success: Boolean
}

type Mutation {
  createUser(user: CreateUser): User!,
  createRole(role: CreateRole): Role!,
  createCustomer(customer: CreateCustomer): Customer!,
  toogleUserToRole(userId: Int, roleId: Int): MutationResult
}
`;

export default makeExecutableSchema({
  typeDefs: schema,
  resolvers: resolvers
})
