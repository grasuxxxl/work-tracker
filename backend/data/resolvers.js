// @flow
import  { ValidationError } from '../../common/errors/index.js';

type Context = {
  transaction: Knex$Transaction,
  users: ?Array<Schema$User>
}

function unwrap<T>(item: ?T): T {
  if (item === undefined || item === null) {
    throw new ValidationError();
  }
  return item;
}

import type { User } from '../models/index.js';
import { validateUser, validateRole, validateCustomer } from '../../common/validations/index.js';
import { repoWithTransaction } from '../repos/index.js';
import { getAllUsers, getPagedUsers, addUser } from '../services/users';

const resolvers = {
  Query: {
    totalUsers (obj: any, args: any, { transaction }: Context, info: any): Promise<number> {
      return repoWithTransaction(transaction).users.getAllCount();
    },
    usersPaged (obj: any,
      { startIndex, endIndex }: { startIndex: number, endIndex: number },
      { transaction }: Context,
      info: any): Promise<Array<Schema$User>> {

      const users = getPagedUsers(repoWithTransaction(transaction), startIndex, endIndex);
      return users;
    },

    users (obj: any, args: any, { transaction }: Context, info: any): Promise<Array<Schema$User>> {
      const users = getAllUsers(repoWithTransaction(transaction));
      return users;
    },

    roles (obj: any, args: any, { transaction, users }: Context) {
      if (users) {
          users.forEach(u => u);
      }

      return repoWithTransaction(transaction).roles.getAll();
    },
    customers (obj: any, args: any, { transaction }: Context) {
      return repoWithTransaction(transaction).customers.getAll();
    }
  },
  User: {
    roles({ id }: Schema$User, args: any, { transaction }: Context, info: any): Promise<Array<Schema$Role>> {
      const roles = repoWithTransaction(transaction).users.getRoles({ id });
      return roles;
    }
  },
  Mutation: {
    createUser(_: any, parameters: CreateUserMutationVariables, { transaction }: Context): Promise<User> {
      const user = unwrap(parameters.user);
      validateUser(user);
      let { email, password, phone } = user;
      const repo = repoWithTransaction(transaction);

      return addUser(repo, user);
    },

    createRole(_: any, parameters: CreateRoleMutationVariables, { transaction }: Context) {
      const role = unwrap(parameters.role);
      validateRole(role);
      const { name } = role;
      const repo = repoWithTransaction(transaction);

      return repo.roles.add({ name })
        .then(repo.roles.getById);
    },

    createCustomer(_: any, parameters: CreateCustomerMutationVariables, { transaction }: Context) {
      const customer = unwrap(parameters.customer);
      validateCustomer(customer);
      const { name } = customer;
      const repo = repoWithTransaction(transaction);

      return repo.customers.add({ name })
        .then(repo.customers.getById);
    },

    toogleUserToRole(_: any, { userId, roleId }: any, { transaction }: Context) {
      let createUserInRole = { "user_id": userId, "role_id": roleId };
      const repo = repoWithTransaction(transaction);

      return repo.userInRole.toogle(createUserInRole)
        .then(function () {
          return { success: true };
        })
        .catch(function (error) {
          console.log(error);
          return { success: false };
        });
    }
  }
};

export default resolvers;
