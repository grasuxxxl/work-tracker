// @flow
import { exit } from 'process';
import { writeFileSync } from 'fs';
import find from 'find';
import { join } from 'path';
import {
  buildASTSchema,
  graphql,
  parse,
  buildClientSchema,
  validate,
  specifiedRules
} from 'graphql';
import { NoUnusedFragments } from 'graphql/validation/rules/NoUnusedFragments';

import { introspectionQuery } from 'graphql/utilities';
import { schema } from '../backend/data/schema.js';
import { generateSource as generateFlowSource } from 'apollo-codegen/lib/flow';
import { loadAndMergeQueryDocuments } from 'apollo-codegen/lib/loading';
import { validateQueryDocument, NoAnonymousQueries, NoTypenameAlias } from 'apollo-codegen/lib/validation';
import { compileToIR } from 'apollo-codegen/lib/compilation';

const queryFlowTypesFilePath: string = './flow-typed/local/queries.js';
var inputPaths = [];
const options = {};

async function introspect(schemaContents: string): any {
  const astSchema = buildASTSchema(parse(schemaContents));
  return await graphql(astSchema, introspectionQuery);
}

async function generateQueryFlowTypes() {
  var schemaIntrospection = await introspect(schema);
  const clientSchema = buildClientSchema(schemaIntrospection.data);

  const document = loadAndMergeQueryDocuments(inputPaths);
  validateQueryDocument(clientSchema, document);

  var context = compileToIR(clientSchema, document, options);
  Object.assign(context, options);

  const output = generateFlowSource(context, options);
  writeFileSync(queryFlowTypesFilePath, output);
}

find.file(/\.gql$/, join(__dirname, '..'), function (files) {
  inputPaths = files;
  generateQueryFlowTypes()
    .then(r => exit())
    .catch(function (error) {
      console.log(error);
      exit()
    });
});
