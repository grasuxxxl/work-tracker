import repo from '../backend/repos/index.js';
import R from 'ramda';


let users = [
  {email: 'vasile@gmail.com'},
  {email: 'ion@gmail.com'},
  {email: 'ghita@gmail.com'},
  {email: 'doru@gmail.com'},
  {email: 'bobonete@gmail.com'},
  {email: 'giani@gmail.com'},
  {email: 'gianina@gmail.com'},
  {email: 'vasile@gmail.com'},
  {email: 'ion@gmail.com'},
  {email: 'ghita@gmail.com'},
  {email: 'doru@gmail.com'},
  {email: 'bobonete@gmail.com'},
  {email: 'giani@gmail.com'},
  {email: 'gianina@gmail.com'},
  {email: 'vasile@gmail.com'},
  {email: 'ion@gmail.com'},
  {email: 'ghita@gmail.com'},
  {email: 'doru@gmail.com'},
  {email: 'bobonete@gmail.com'},
  {email: 'giani@gmail.com'},
  {email: 'gianina@gmail.com'},
  {email: 'vasile@gmail.com'},
  {email: 'ion@gmail.com'},
  {email: 'ghita@gmail.com'},
  {email: 'doru@gmail.com'},
  {email: 'bobonete@gmail.com'},
  {email: 'giani@gmail.com'},
  {email: 'gianina@gmail.com'},
  {email: 'vasile@gmail.com'},
  {email: 'ion@gmail.com'},
  {email: 'ghita@gmail.com'},
  {email: 'doru@gmail.com'},
  {email: 'bobonete@gmail.com'},
  {email: 'giani@gmail.com'},
  {email: 'gianina@gmail.com'}
];

function addUser(user) {
  return repo.users.add(user);
}

let createUsers = R.pipe(
  R.map(user => addUser.bind(null, user)),
  R.map(userAdd => userAdd())
)(users);

Promise.all(createUsers)
  .then(function (values) {
    console.log(values);
  });
